from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.sqlite'
db=SQLAlchemy(app)
CORS(app,supports_credentials=True)

class historiy(db.Model):
    __tablename__='historiy'
    id=db.Column(db.Integer,primary_key=True,autoincrement=True)
    jisuan=db.Column(db.String(300),nullable=True)
    result=db.Column(db.String(300),nullable=True)

with app.app_context():
    db.create_all()

@app.route('/',methods=['POST'])
def insert_result():
    data=request.get_json()
    try:
        jisuan=data['jisuan']
        result=data['result']
    except KeyError:
        return jsonify({'msg':'error','code':400},),400
    historiy_list=historiy(jisuan=f"{jisuan}",result=result)
    db.session.add(historiy_list)
    db.session.commit()
    return {'code':200}

@app.route('/select_data')
def selete_data():
    data=historiy.query.filter_by().all()
    result_data=[]
    for i in data:
        result_data.append({'id':i.id,'jisuan':eval(i.jisuan),'result':i.result})
    return jsonify(data=result_data,code=200)
    
if __name__=='__main__':
    app.run(debug=True)