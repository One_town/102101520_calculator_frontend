## 项目描述

​	该项目实现的是一个前后端分离的科学计算器，通过执行backend中的main.py运行，再打开calculator.html即可查看历史记录。

### 功能简介

​	1.加减乘除等基本运算功能。

​	2."AC" 和"←"清零回退功能

​	3.查询历史记录的功能

​	4.利率计算器功能

### 作业链接

https://bbs.csdn.net/topics/617447505

### 博客链接
https://blog.csdn.net/One_town/article/details/133980422?csdn_share_tail=%7B%22type%22%3A%22blog%22%2C%22rType%22%3A%22article%22%2C%22rId%22%3A%22133980422%22%2C%22source%22%3A%22One_town%22%7D
